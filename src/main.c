#include "mem.h"
#include <string.h>

int* intArr;
int* bigIntArr;
int* bigIntArr2;
char* str;
char* str1;
char* str2;
int64_t* BIG_ARR;

static void print_line()
{
	printf("\n");
	for(int i = 0; i<50;i++)
		printf("_");
	printf("\n\n");
}

void t1()
{
	size_t intCount = 20;
	
	intArr = (int*)_malloc(sizeof(int) * intCount);
	printf("%p\n\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		intArr[i]=i+1;
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
	
	print_line();
}

void t2()
{
	size_t intCount = 20;
	_free(intArr);
	intArr = (int*)_malloc(sizeof(int) * intCount);
	printf("%p\n\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
	
	print_line();
}

void t3()
{
	str = (char*)_malloc(7);
	printf("%p\n\n", (void*)str);
	str[0] = 's'; str[1] = 't'; str[2] = 'r'; str[3] = 'o'; str[4] = 'k'; str[5] ='a'; str[6] = '\0';
	printf("%s", str);
	
	print_line();
}

void t4()
{
	size_t intCount = 20;
	printf("%p\n\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
	
	print_line();
}

void t5()
{
	char str_tmp[30] = "ab";
	_free(intArr);
	str1 = (char*)_malloc(strlen(str_tmp)+1);
	str2 = (char*)_malloc(strlen(str_tmp)+1);
	for(size_t i = 0; i < strlen(str_tmp); i++)
	{
		str1[i] = str_tmp[i];
		str2[i] = str_tmp[strlen(str_tmp) - i - 1];
	}
	
	str1[strlen(str_tmp)] = '\0';
	str2[strlen(str_tmp)] = '\0';
	
	printf("%p\n\n", (void*)str1);
	printf("%s", str1);
	
	print_line();
	
	printf("%p\n\n", (void*)str2);
	printf("%s", str2);
	print_line();
}

void free_t1_t5()
{
	_free(intArr);
	_free(str);
	_free(str1);
	_free(str2);
}

void t6()
{
	size_t intCount = 100;
	bigIntArr = (int*)_malloc(sizeof(int)*intCount);
	printf("%p\n\n", (void*)bigIntArr);
	print_line();
	
	bigIntArr2 = (int*)_malloc(sizeof(int)*intCount);
	printf("%p\n\n", (void*)bigIntArr2);
	print_line();
}

void t7()
{
	size_t INT_COUNT = 100000;
	BIG_ARR = (int64_t*)_malloc(sizeof(int64_t)*INT_COUNT);
	printf("%p\n\n", (void*)BIG_ARR);
	print_line();
}

void free_t6_t7()
{
	_free(bigIntArr);
	_free(bigIntArr2);
	_free(BIG_ARR);
}

int main(int arg, char** args)
{
	if(arg > 1)
	{
		size_t n = strlen(args[1]), heap_init_sz = 0;
		
		for(size_t i=0; i < n; i++)
			if(!('0' <= args[1][i] && args[1][i] <= '9'))
				while(true) printf("Oh");
			else
				heap_init_sz = heap_init_sz*10 + args[1][i] - '0';
		heap_init(heap_init_sz);
	}
	else
		heap_init(100);
	t1();
	t2();
	t3();
	t4();
	t5();
	free_t1_t5();
	t6();
	t7();
	free_t6_t7();
	return 0;
}